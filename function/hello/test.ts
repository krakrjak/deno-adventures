import { assertEquals } from "https://deno.land/std@0.92.0/testing/asserts.ts";

// One throw away test like the examples
Deno.test({
  name: "is 3 still 3?",
  fn: () => {
    const x = 1 + 2;
    assertEquals(x, 3);
  },
});
