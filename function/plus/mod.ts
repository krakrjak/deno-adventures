import {
  Response as ServerResponse,
  ServerRequest,
} from "https://deno.land/std/http/server.ts";

export default (_request: ServerRequest): ServerResponse => {
  const body = "Hello World";
  return {
    body,
  } as ServerResponse;
};
