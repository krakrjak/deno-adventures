# Deno Adventures

Exploring a new toolkit is always interesting.

[The Manual](https://deno.land/manual)

## Getting going

To make use of these adventures you will need to
[Install `deno`](https://deno.land/#installation).

To test packaging and deployment
[Install faas-cli](https://github.com/openfaas/faas-cli/blob/master/README.md#get-started-install-the-cli)

## Test `deno` Kit

Make sure you can get the help working in your terminal:

```sh
$ deno --help
    deno ...
    A secure JavaScript and TypeScript runtime
    ...
```

For your next trick see if the example works for you.

```sh
$ deno run https://deno.land/std/examples/welcome.ts
    Download https://deno.land/std/examples/welcome.ts
    Warning Implicitly using latest version (0.92.0) for https://deno.land/std/examples/welcome.ts
    Download https://deno.land/std@0.92.0/examples/welcome.ts
    Check https://deno.land/std/examples/welcome.ts
    Welcome to Deno!
```

## Run

To have deno run the app point it at the entrypoint you want, say:

```sh
$ deno run deno/app.ts
```

And it should go on its merry way caching dependencies for you. Handy eh?

## Deno Resources

[The Manual](https://deno.land/manual)
[freeCodeCamp - The Deno Handbook](https://www.freecodecamp.org/news/the-deno-handbook/)
[InfoQ - Demo Introduction Practical
Examples](https://www.infoq.com/articles/deno-introduction-practical-examples/)

[Deploy to Cloud Run](https://medium.com/google-cloud/deno-on-cloud-run-89ae64d1664d)

[Serverless API with Deno and Begin - Part
1](https://dev.to/pchinjr/serverless-api-with-deno-and-begin-part-1-5aoo)
[Serverless API with Deno and Begin - Part
1](https://dev.to/pchinjr/serverless-web-api-with-deno-and-begin-part-2-5fjg)
