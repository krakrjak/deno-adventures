let GitLab =
      https://raw.githubusercontent.com/krakrjak/dhall-gitlab-ci/rules-predicates/package.dhall

let Prelude = GitLab.Prelude

let List/map = Prelude.List.map

let Map = Prelude.Map.Type

let Job = GitLab.Job.Type

let NeedEntry = GitLab.NeedEntry

let renderTop = GitLab.Top.toJSON

let targets = [ "multiply" ]

let gitlab_workflow = < branch | protected | release >

let kaniko_script_release =
      λ(target : Text) →
        ''
        /kaniko/executor --context "$CI_PROJECT_DIR/build/${target}" \
        --cache=true \
        --cache-repo $CI_REGISTRY_IMAGE \
        --destination $CI_REGISTRY_IMAGE/${target} \
        --destination $CI_REGISTRY_IMAGE/${target}:$CI_COMMIT_TAG \
        --destination $CI_REGISTRY_IMAGE/${target}:$CI_COMMIT_SHA
        ''

let kaniko_script_protected =
      λ(target : Text) →
        ''
        /kaniko/executor --context "$CI_PROJECT_DIR/build/${target}" \
        --cache=true \
        --cache-repo $CI_REGISTRY_IMAGE \
        --destination $CI_REGISTRY_IMAGE/${target}/$CI_COMMIT_REF_SLUG \
        --destination $CI_REGISTRY_IMAGE/${target}/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA \
        --destination $CI_REGISTRY_IMAGE/${target}/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
        ''

let kaniko_script_nopush =
      λ(target : Text) →
        ''
        /kaniko/executor --context "$CI_PROJECT_DIR/build/${target}" \
        --destination $CI_REGISTRY_IMAGE/${target}/$CI_COMMIT_REF_SLUG \
        --no-push
        ''

let branch_rule = "\$CI_COMMIT_BRANCH"

let release_rule = "\$CI_COMMIT_TAG"

let protected_rule = "\$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH"

let mkJob =
      λ(workflow : gitlab_workflow) →
      λ(target : Text) →
        GitLab.Job::{
        , stage = Some "build"
        , image = Some
          { name = "gcr.io/kaniko-project/executor:debug"
          , entrypoint = Some [ "" ]
          }
        , script =
          [ "mkdir -p /kaniko/.docker"
          , ''
            echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
            ''
          , merge
              { branch = kaniko_script_nopush target
              , protected = kaniko_script_protected target
              , release = kaniko_script_release target
              }
              workflow
          ]
        , needs = Some
          [ NeedEntry.Type.Pipeline
              { pipeline = "\$PARENT_PIPELINE_ID", job = "generate-config" }
          ]
        , rules = Some
          [ { `if` =
                merge
                  { branch = branch_rule
                  , protected = protected_rule
                  , release = release_rule
                  }
                  workflow
            }
          ]
        }

let branchJobs
    : List { mapKey : Text, mapValue : Job }
    = List/map
        Text
        { mapKey : Text, mapValue : Job }
        ( λ(target : Text) →
            { mapKey = "${target}:no-push"
            , mapValue = mkJob gitlab_workflow.branch target
            }
        )
        targets

let protectedJobs
    : List { mapKey : Text, mapValue : Job }
    = List/map
        Text
        { mapKey : Text, mapValue : Job }
        ( λ(target : Text) →
            { mapKey = "${target}/default"
            , mapValue = mkJob gitlab_workflow.protected target
            }
        )
        targets

let releaseJobs
    : List { mapKey : Text, mapValue : Job }
    = List/map
        Text
        { mapKey : Text, mapValue : Job }
        ( λ(target : Text) →
            { mapKey = "${target}:release"
            , mapValue = mkJob gitlab_workflow.release target
            }
        )
        targets

let jobMap
    : Map Text Job
    = branchJobs # protectedJobs # releaseJobs

let top = GitLab.Top::{ jobs = jobMap }

in  Prelude.JSON.renderYAML (renderTop top)
